import os
import sys
import operator
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
import shutil

# пример именования бекапов
# modos189.2017-01-26.tar


# возвращает первый бекап в месяце
def get_first_base(req):
    min_date = None
    for file in target_files:
        data = file.split(".")

        # как минимум 3 точки в названии
        # совпадает имя пользователя
        # совпадает месяц
        # дата меньше или равна запрошенной
        if len(data) >= 3 and req[0] == data[0] and req[1][:-2] == data[1][:-2] and req[1] <= data[1]:
            if min_date is None or min_date > data[1]:
                min_date = data[1]

    return min_date


def action(source, base_date=None):
    logging.info("Подготовка " + source[0] + " " + source[1] + " с базовым " + str(base_date))

    # создаём файл signature для базового бекапа если отсутсвует
    if not os.path.exists(target_directory + source[0] + "." + source[1] + ".signature") and base_date is None:
        os.system('rdiff signature "' + source_directory + source[0] + '.' + source[1] + '.tar"'
                  ' "'+target_directory + source[0] + '.' + source[1] + '.signature"')
        logging.info("Создан " + source[0] + '.' + source[1] + ".signature")

    # базовые копируем, а для инкрементальных создаём файл с разницей
    if base_date is None:
        logging.info("Копирование .tar в папку назначения")
        shutil.copy2(source_directory + source[0] + '.' + source[1] + '.tar', target_directory)
    else:
        if not os.path.exists(target_directory + source[0] + "." + source[1] + ".rdiff"):
            os.system('rdiff delta "' + target_directory + source[0] + '.' + base_date + '.signature"'
                      ' "' + source_directory + source[0] + '.' + source[1] + '.tar"'
                      ' "' + target_directory + source[0] + '.' + source[1] + '.rdiff"')
            logging.info("Создан " + source[0] + '.' + source[1] + ".rdiff")

    logging.info("")


# поиск архивов, с которыми будем работать
def preparation():
    source_backups = {}

    # перебор всех бекапов
    for file in source_files:
        # пропуск, если не .tar
        if not file.endswith("tar"):
            continue

        data = file.split(".")
        # пропуск если более 3-х точек в названии. Ну, малоли
        if len(data) != 3:
            continue

        # пропуск уже скопированных бэкапов
        if os.path.exists(target_directory + data[0] + "." + data[1] + ".tar"):
            continue

        source_backups[data[0]+data[1]] = data

    # получаем отсортированный по дате список бекапов
    source_backups = sorted(source_backups.items(), key=operator.itemgetter(0))

    first = [None, None]
    # перебор бекапов, с которыми будем работать
    for key, data in source_backups:
        if data[0] != first[0]:
            first[0] = data[0]
            first[1] = data[1]

        # полный бекап первого числа кадого месяца
        date_of_base_backup = data[1][:-2] + "01"

        # ищем базовый бекап в папке назначения
        if os.path.exists(target_directory + data[0] + "." + date_of_base_backup + ".tar"):
            action(data, date_of_base_backup)
        else:
            # если создан в первый день месяца, то должен стать базовым
            if data[2].endswith("01"):
                action(data)
            else:
                # бакап за первый день месяца отсутствует
                # ищём первый среди архивов в папке назначения
                # иначе среди очереди
                first_performed = get_first_base(data)
                if first_performed is None:
                    # очередь. совпадают год и месяц, но не число
                    if first[1][:-2] == data[1][:-2] and first[1] != data[1]:
                        first_performed = first[1]

                action(data, first_performed)


def delete_old(months):
    months = int(months)
    # если пользователь указал 0 - значит не требуется удалять
    if months == 0:
        return

    allow = datetime.now() + relativedelta(months=-months)
    allow = str(allow.date())[:-2] + "01"
    logging.debug("Delete all before " + allow)
    for file in target_files:
        data = file.split(".")

        if len(data) >= 3 and data[1] < allow:
            logging.debug("EXTERMINATE " + str(data[0]) + "." + str(data[1]))
            os.remove(target_directory + data[0] + "." + data[1] + "." + data[2])


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("USAGE:\npython3 vestacp-incremental-backup.py "
              "diff SOURCE_DIR TARGET_DIR MONTHS_TO_KEEP(default 0 - do not remove)")
    else:

        logger = logging.basicConfig(
            level=logging.INFO
        )

        source_directory = sys.argv[2]
        target_directory = sys.argv[3]

        if len(sys.argv) > 4:
            months_to_keep = sys.argv[4]
        else:
            months_to_keep = 0

        source_files = os.listdir(source_directory)
        target_files = os.listdir(target_directory)

        preparation()

        delete_old(months_to_keep)

        logging.info("Готово! Пример восстановления:\nrdiff patch base-backup.2017-01-01.tar"
                     " incremental-backup.2017-01-06.rdiff incremental-backup.2017-01-06.tar")
